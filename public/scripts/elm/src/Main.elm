module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode
import Models exposing (Model)
import Msg exposing (..)
import Ports

main : Program () Model Msg
main = Browser.element
       { init = init
       , subscriptions = subscriptions
       , update = update
       , view = view
       }

------------------------
init : () -> (Model, Cmd Msg)
init _ = ( Models.init
         , Cmd.none
         )

         
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Ports.toElm UpdateString
              ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        SendString str -> { model | str = str }
                          |> \m -> ( m, Ports.toReason m.str )
        UpdateString val -> { model | str = val }
                       |> \m -> (m, Cmd.none)
                   
view : Model -> Html Msg
view model =
    div [ class "elm-parent" ]
        [ h2 [ class "h2" ] [ text "Controlled by Elm" ]
        , input [ placeholder "enter some text"
                , type_ "text"
                , onInput SendString
                , value model.str
                ] []
        ]

{---------------------------helpers----------------------------}
        
